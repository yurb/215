Unless otherwise noted, content of this project is licensed under
[Creative Commons Attribution-ShareAlike 4.0 International][cc-by-sa] license.

Please also see [samples.md](samples/samples.md).

[cc-by-sa]: https://creativecommons.org/licenses/by-sa/4.0/

/* Cleanup */
s = Server.default;
s.freeAll;
Buffer.freeAll;
Window.closeAll;

/* Init MIDI */
MIDIClient.init;

/* Run */
s.waitForBoot {
	fork {
	[
		"common.scd",
		"flow.scd",
		"light.scd"
	].do({|i| i.loadRelative; s.sync; });
	};
};

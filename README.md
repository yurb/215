Code and samples for an interactive audio instalation called
"2kg15min". The project has more than one iteration; each of them is
marked with a corresponding tag. The master branch represents the most
recent one.

Copyright 2015 by Yury Bulka, some samples by freesound.org
contributors (see [samples.md](samples/samples.md)).

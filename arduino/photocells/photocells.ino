/* Read photoresistors.
 * License: cc0
 */

int sensorPins[] = {A0, A1, A2, A3, A4};
int numSensors;
int interval;

void setup() {
  // Setup serial link with the computer
  Serial.begin(9600);
  numSensors = sizeof(sensorPins) / sizeof(int);
  interval = 1000/25;
}
    
void loop() {
  int sensorVal = 0;
  for (int i = 0; i < numSensors; i++) {
    sensorVal = analogRead(sensorPins[i]);
    
    /* For each sensor write a line of the format:
       1. OSC-like uri of the sensor
       2. The sequential number of the sensor
       3. the sensor value
       The fields are separated by space.
     */
    Serial.print("photocell");
    Serial.print(" ");
    Serial.print(i);
    Serial.print(" ");
    Serial.println(sensorVal);
  }

  delay(interval);
}
